from collections import namedtuple
import numpy as np

Level = namedtuple("Level", "size row_hints col_hints")

def load(filename):

    state = "seek rows"
    rows = []
    cols = []

    with open(filename, "r") as f:
        for l in f:
            l = l.strip()
            # print(state, "\t", l)

            if state == "seek rows":

                if l != "rows":
                    raise Exception("malformed level file")
                state = "load row"
                continue

            if state == "load row":

                if l == "columns":
                    state = "load column"
                    continue

                tokens = l.split(" ")
                row = []
                for t in tokens:
                    row.append(int(t))

                rows.append(np.array(row, dtype=np.uint32))

            if state == "load column":

                if l == "":
                    continue

                tokens = l.split(" ")
                col = []
                for t in tokens:
                    col.append(int(t))

                cols.append(np.array(col, dtype=np.uint32))

    return Level((len(rows), len(cols)), rows, cols)

