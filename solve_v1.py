#! /usr/bin/env python3

import numpy as np
import imageio
from sys import argv

import level

def hint_sum(hints):
    return np.sum(hints) + len(hints) - 1

def fill_constrained(level, solution, row_wise=True):

    if row_wise:
        on_dim = 0
        off_dim = 1
        color = 1
        hints_list = level.row_hints
    else:
        on_dim = 1
        off_dim = 0
        color = 2
        hints_list = level.col_hints

    for x in range(level.size[on_dim]):
        
        hints = hints_list[x]
        slack = level.size[off_dim] - hint_sum(hints)
        
        if slack < np.max(hints):
            hint = 0
            hint_idx = 0

            skip_for_space = False
            for y in range(level.size[off_dim]):

                if skip_for_space:
                    skip_for_space = False
                    continue

                if hint >= hints[hint_idx]:
                    if hint_idx >= len(hints)-1:
                        break
                    hint_idx += 1
                    hint = 0
                    skip_for_space = True

                if row_wise:
                    r = x
                    c = y
                else:
                    r = y
                    c = x

                if hint >= slack:
                    solution[r,c,:] = black_level
                else:
                    solution[r,c,color] = hint_idx

                hint += 1

if __name__ == "__main__":

    # initialize I/O data
    current_level = level.load(argv[1])
    sz = current_level.size
    solution = np.full((sz[0], sz[1], 3), -1, dtype=np.int8)

    black_level = np.max(current_level.size)/2


    # fill in the guaranteed spaces
    fill_constrained(current_level, solution, True)
    fill_constrained(current_level, solution, False)
                

    # save the solution
#    solution = -solution
    solution -= np.min(solution)
    solution = (solution * 255.0/np.max(solution)).astype(np.uint8)
    print(solution)
    imageio.imwrite(argv[2], solution)
