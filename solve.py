#! /usr/bin/env python3

# occupancy - how much the hints of a line 'occupy' that line, the amount by
# which they constrain it

import numpy as np
import imageio
from sys import argv

import level

# definition of how much the hints "occupy" the row (N hints -> N-1 spaces
# required)
def occupancy(hints):
    return np.int32(hints.sum() + hints.size - 1)

# logical implication operator
# ~P | Q
def implies(a, b):
    return np.logical_or(np.logical_not(a), b)

# recursively generate all possible solutions for a given list of hints and a
# row width / column height
def generate_candidates(hints, width):

    if 0 == hints.size:
        return [ np.zeros(width, dtype=np.bool) ]

    candidates = []

    slack = width - occupancy(hints)
    for i in range(slack+1):

        # possible arrangement for this hint
        a = np.zeros(i, dtype=np.bool)
        b = np.ones(hints[0], dtype=np.bool)
        prefix = np.append(a, b)
        if hints.size > 1: # not the last hint in the row
            prefix = np.append(prefix, np.zeros(1, dtype=np.bool))

        suffixes = generate_candidates(hints[1:], width - prefix.size)

        for s in suffixes:
            candidates.append(np.append(prefix, s))

    return np.array(candidates)

# using current row state and the level's hints, fill in guaranteed spaces
def iterate_row(hints, current, candidates):

    # if all hints are satisfied, this row is done and all -1 should be 0
    if hints.sum() == np.sum(current == 1):
        current[current < 0] = 0
        return current, None, True

    # For all solutions that agree with the current solution (0 stays 0, 1 stays
    # 1, -1 can become either), any tile which is 1 in every possible solution
    # is 1 in the ultimate solution. The same is true for '0' tiles.

    solutions = []

    for c in candidates:
        if np.all(implies(current == 1, c)) and \
            np.all(implies(current == 0, np.logical_not(c))):

            solutions.append(c)

    soln_array = np.array(solutions)

    all_true = np.all(soln_array, axis=0)
    all_false = np.all(np.logical_not(soln_array), axis=0)

    current[all_true] = 1
    current[all_false] = 0

    return current, solutions, np.all(current != -1)

# run only when invoked directly, not via another file
if __name__ == "__main__":

    # initialize I/O data
    current_level = level.load(argv[1])
    sz = current_level.size
    solution = np.full((sz[0], sz[1]), -1, dtype=np.int8)

    # iterate rows, columns, rows, columns, ...
    rows_unsolved = list(range(sz[0]))
    cols_unsolved = list(range(sz[1]))

    # store the generated solutions per-row and per-column
    # keys: row or height+column
    solutions_cache = {}
    for r in rows_unsolved:
        solutions_cache[r] = generate_candidates(current_level.row_hints[r], \
            current_level.size[0])
    for c in cols_unsolved:
        solutions_cache[current_level.size[0]+c] = \
            generate_candidates(current_level.col_hints[c], 
            current_level.size[1])

    iters = 0
    while len(rows_unsolved) > 0 or len(cols_unsolved) > 0:

        print(iters)

        if iters > 100:
            print("failed to solve in", iters, "iterations.")
            break

        for row in rows_unsolved:

            solution[row], solutions_cache[row], is_solved = \
                iterate_row(current_level.row_hints[row], solution[row],
                solutions_cache[row])
            if is_solved:
                rows_unsolved.remove(row)

        for col in cols_unsolved:
            solution[:,col], solutions_cache[current_level.size[0]+col], is_solved = \
                iterate_row(current_level.col_hints[col], solution[:,col],
                solutions_cache[current_level.size[0]+col])
            if is_solved:
                cols_unsolved.remove(col)

        iters += 1

    # save the solution as an image
    #solution = -solution
    solution -= np.min(solution)
    solution = (solution * 255.0/np.max(solution)).astype(np.uint8)
    #print(solution)
    imageio.imwrite(argv[2], solution)
